import glob
import pandas as pd
import numpy as np
import json
import sys
sys.path.append("../NLP/")
import nlp_functions
PATH="/home/chabert/Projets/Tourisme/Data/tourisme"

INFINI=999999999999999
class Data_POI():
    """
    class to stock data from data tourisme
    """
    def __init__(self,path,searchwords,critere):
        self.path=path
        self.stored_data={}
        self.stored_data['POI']={}
        self.critere=critere
        self.searchwords=searchwords
        self.name_file=self.critere+'.'+'data.json'
        print("file will be",self.name_file)
    def get_list_of_data_files(self):
        """
        gets list of csv files in path
        """
        self.liste_of_data_files=(glob.glob(self.path+"/datatourisme."+self.critere+"*.csv"))
        print("FILES:",self.liste_of_data_files)
    def write(self):
        print("in write")
        with open(self.name_file, 'w') as f:
            print("----------------------------------------------->Now writing to",self.name_file)
            json.dump(self.stored_data, f,ensure_ascii=False)
    def clean_string(self,s):
        """
        cleans a string for NLP
        """
        list_of_bad_chars=['\n','...',',','.','!','?','\r','…','"',"//","-","\t",'"','\"','\"']
        sout=s
        for c in list_of_bad_chars:
            sout=sout.replace(c," ")
        return(sout)
    def analyse_data_files(self):
        """
        gets content of files in path
        """
        for f in self.liste_of_data_files:
            print("=========================",f)
            self.type=f.split(".")[1]
            self.stored_data['POI'][self.type]={}
            data = pd.read_csv(f)
            #print(data.keys())
            length=len(data)
            cpt=0
            for e in range(length):
                cpt=cpt+1
                try:
                    nom=(str)(data.loc[e,'Nom_du_POI']).lower()
                    keywords_nom=nlp_functions.Keywords(nom,'fr')
                    keywords_nom.clean()
                    keywords_nom.get(True,1,1,1,1,1)
                    description=self.clean_string((str)(data.loc[e,'Description']).lower())
                    keywords_description=nlp_functions.Keywords(description,'fr')
                    keywords_description.clean()
                    keywords_description.get(True,1,1,1,1,1)
                    adresse=(str)(data.loc[e,'Adresse_postale']).lower()
                    Latitude=(str)(data.loc[e,'Latitude'])
                    Longitude=(str)(data.loc[e,'Longitude'])
                    Code_postal_et_commune=(str)(data.loc[e,'Code_postal_et_commune'])
                    try:
                        Periodes_regroupees=(str)(data.loc[e,'Periodes_regroupees'])
                    except:
                        Periodes_regroupees="x"
                    try:
                        code_postal=(int)(Code_postal_et_commune.split("#")[0])
                    except:
                        code_postal=Code_postal_et_commune.split("#")[0]
                    try:
                        ville=Code_postal_et_commune.split("#")[1]
                    except:
                        ville="x"
                    all_keywords=[]
                    for k in keywords_nom.common_nouns:
                        if not(k in all_keywords):
                            all_keywords.append(k)
                    for k in keywords_nom.common_adjs:
                        if not(k in all_keywords):
                            all_keywords.append(k)
                    for k in keywords_nom.common_propns :
                        if not(k in all_keywords):
                            all_keywords.append(k)
                    for k in keywords_description.common_nouns:
                        if not(k in all_keywords):
                            all_keywords.append(k)
                    for k in keywords_description.common_adjs:
                        if not(k in all_keywords):
                            all_keywords.append(k)  
                    for k in keywords_description.common_propns :
                        if not(k in all_keywords):
                            all_keywords.append(k)
                    
                    counter=0
                    if len(self.searchwords)>0:
                        for w in self.searchwords:
                            if nom.find(w)>=0 or description.find(w)>=0:
                                counter+=1
                    if counter>0 or len(self.searchwords)==0:
                        if cpt%100==0:
                            print(nom,cpt,"/",length,Periodes_regroupees)
                        #print("KEYWORDS",all_keywords,"\n*************************")
                        try:
                            Latitude=(float)(Latitude)
                        except:
                            Latitude=-INFINI

                        try:
                            Longitude=(float)(Longitude)
                        except:
                            Longitude=-INFINI
                        self.stored_data['POI'][self.type][nom]={'code_postal':code_postal,'ville':ville,'adresse':adresse,'description':description,'latitude':Latitude,'longitude':Longitude,'keywords':all_keywords,"Periodes_regroupees":Periodes_regroupees}
                except:
                    print("BOOM ON",nom)
            #print(keys)
            #print(df[["Nom_du_POI","Latitude","Longitude","Description"]])
            # sub ='Circuit'
  
            # start = 0
  
            # data["Indexes"]= data["Nom_du_POI"].str.find(sub, start) 
  
            # print("DATA:",data)







