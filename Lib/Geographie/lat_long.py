
# -*- coding: utf-8 -*-
import math
import json
import datetime

PATH_JSON="../POI/"

def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d



def get_pois_from_jsons(keyword,lat,long,radius=50):
    """
    get all pois from the four json files
    """
    dict_out={}
    mot=keyword
    l1=get_pois(PATH_JSON+"fma.data.json","fma",mot,lat,long,radius)
    l2=get_pois(PATH_JSON+"tour.data.json","tour",mot,lat,long,radius)
    l3=get_pois(PATH_JSON+"product.data.json","product",mot,lat,long,radius)
    l4=get_pois(PATH_JSON+"place.data.json","place",mot,lat,long,radius)
    for l in l1:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    for l in l2:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    for l in l3:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    for l in l4:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    return(dict_out)
def get_pois(json_file,key,keyword,lat,long,radius=50):
    """
    get all pois with keyword within radius in km
    """
    t0=datetime.datetime.now()

    lout=[]
    
    with open(json_file) as json_file:
        data = json.load(json_file)
        for p in data['POI'][key]:
            latitude_poi=data['POI'][key][p]['latitude']
            longitude_poi=data['POI'][key][p]['longitude']
            keywords=data['POI'][key][p]['keywords']
            description=data['POI'][key][p]['description']
            ville=data['POI'][key][p]['ville']
            code_postal=data['POI'][key][p]['code_postal']
            adresse=data['POI'][key][p]['adresse']
            try:
                Periodes_regroupees=data['POI'][key][p]['Periodes_regroupees']
            except:
                Periodes_regroupees="x"
            dist=distance((latitude_poi,longitude_poi),(lat,long))
            found=0
            for k in keywords:
                if k.find(keyword)>=0:
                    found+=1
                    t=datetime.datetime.now()
                    dt=t-t0
            if dist<radius and found>0:
                lout.append([p,dist,keywords,description,ville,code_postal,adresse,Periodes_regroupees,latitude_poi,longitude_poi,dt])
    return(lout)


d=get_pois_from_jsons("forêt",43.498596,1.310822,radius=50)

for k in d:
    print(k,d[k]['nom'],d[k]['dt'])


