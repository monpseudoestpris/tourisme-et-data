from google_images_download import google_images_download   #importing the library

import os
from PIL import Image


def get_images(keyword,number=10):

    response = google_images_download.googleimagesdownload()   #class instantiation

    arguments = {"keywords":keyword,"limit":number,"print_urls":True}   #creating list of arguments
    paths = response.download(arguments)   #passing the arguments to the function

    liste=paths[0][keyword]
    for f in liste:
        print(f)

        try :
            with Image.open(f) as im:
                print('ok')
        except :
            print("removing",f)
            os.remove(f)



