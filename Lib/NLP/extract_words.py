import spacy
from collections import Counter




text=[x for x in open("raft.txt").readlines()]
text_all=""
for t in text:
    text_all=text_all+t.replace("\n","")
import nlp_functions


k=nlp_functions.Keywords(text_all,'fr')
k.clean()
k.get(True)
print(k.common_nouns)
print(k.common_adjs)
print(k.common_propns)
print(k.common_verbs)
print(k.common_symbols)