
import spacy
from collections import Counter
from spacy.lang.en.stop_words import STOP_WORDS as STOP_FR
from spacy.lang.fr.stop_words import STOP_WORDS as STOP_EN
from spacy.matcher import Matcher
import itertools
import numpy as np
import random
NLP_FR = spacy.load('fr_core_news_lg')
from gensim.models import KeyedVectors
import pandas as pd
from gensim.corpora.dictionary import Dictionary
from gensim.models import LdaModel
# Usual imports
import pandas as pd
from tqdm import tqdm
import string
import matplotlib.pyplot as plt
from sklearn.decomposition import NMF, LatentDirichletAllocation, TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.manifold import TSNE
import concurrent.futures
import time
import pyLDAvis.sklearn
from pylab import bone, pcolor, colorbar, plot, show, rcParams, savefig
import warnings
from spacy.lang.en import English
from spacy.lang.fr import French
import gensim.corpora as corpora
from sklearn.metrics.pairwise import cosine_similarity
from gensim.matutils import hellinger
from gensim.matutils import kullback_leibler
from gensim.matutils import jaccard



INFINI=999999

#NLP_EN = spacy.load('en_core_web_lg')

import string
class Keywords():
    """
    class returning main words of a text

    """
    def __init__(self,text,language):
        self.nouns=[]
        self.adjs=[]
        self.words=[]
        self.text=text
        if language=="fr":
            self.nlp=NLP_FR
            #self.nlp_en=NLP_EN
    def clean(self):
        """
        cleans text. to be improved
        """
        self.text=self.text.lower()
        for p in string.punctuation:
            self.text=self.text.replace(p," ")
        self.doc=self.nlp(self.text)
        
        #self.doc_en=self.nlp_en(self.text)
    def get(self,lemmatization=True,frequency_noun=2,frequency_adj=1,frequency_propn=1,frequency_verb=1,frequency_symbol=1):
        """
        gets nouns, adjs etc in text

        """
        self.nouns = []
        self.adjs=[]
        self.verbs=[]
        self.propns=[]
        self.symbols=[]
        self.words=[]
        for token in self.doc:
            #print(token,token.pos_)
            if lemmatization==True:
                toadd=token.lemma_
            else:
                toadd=token.orth_
            if not (token.is_punct) and not(token.is_space) and not(token.is_stop) and not (token.__len__()==1):
                    
                if token.pos_=="NOUN":
                    self.nouns.append(toadd )
                    self.words.append(toadd)
                if token.pos_=="ADJ":
                    self.adjs.append(toadd)
                    self.words.append(toadd)
                if token.pos_=="VERB":
                    self.verbs.append(toadd)
                    self.words.append(toadd)
                if token.pos_=="SYM":
                    self.symbols.append(toadd)
                if token.pos_=="PROPN":
                    self.propns.append(toadd)
        self.common_nouns = self.frequency(self.nouns,frequency_noun)
        self.common_adjs = self.frequency(self.adjs,frequency_adj)
        self.common_verbs = self.frequency(self.verbs,frequency_verb)
        self.common_propns=self.frequency(self.propns,frequency_propn)
        self.common_symbols=self.frequency(self.symbols,frequency_symbol)
    def frequency(self,liste,threshold=2):
        """
        returns most frequent tokens

        """
        freq = Counter(liste)
        commons = freq.most_common(100)
        #print("COMMONS",commons)
        out=[x[0] for x in commons if x[1]>=threshold]
        #print("-->",out)
        return(out)
        

class Match():
    def __init__(self,text):
        """
        Matching of rule based patterns
        """
        self.nlp=NLP_FR
        self.text=text
    
    def build_rules(self,rules):
        lout=[]
        for l in rules:
            nbr=len(l)
            ltmp=[]
            for i in range(nbr):
                ltmp.append({"POS":l[i]})
            lout.append(ltmp)
        return(lout)


    def define_rule(self,file_rules):
        """
        definitions de regles en auto avec en entre une liste du type [{"POS": "NOUN"},{"POS":"ADJ"}]
        """
        self.matcher = Matcher(self.nlp.vocab)
        # Add match ID "HelloWorld" with no callback and one pattern
        # self.pattern1 = [ {"POS": "NOUN"},{"POS":"ADJ"}]
        # self.pattern2 = [ {"POS": "ADJ"},{"POS":"NOUN"}]
        # self.pattern3 = [ {"POS": "ADJ"},{"POS":"NOUN"},{"POS":"ADJ"}]
        rules=[x.replace("\n","").split(',')[1:] for x in open(file_rules,'r').readlines()]
        names_rules=[x.replace("\n","").split(',')[0] for x in open(file_rules,'r').readlines()]
  
        rules=self.build_rules(rules)
        for i,e in enumerate(rules):
            name=names_rules[i]
            self.matcher.add(name, None, e)    
        
    def get(self):
        doc = self.nlp(self.text)
        matches = self.matcher(doc)
        if 1==0:
            for match_id, start, end in matches:
                string_id = self.nlp.vocab.strings[match_id]  # Get string representation
                span = doc[start:end]  # The matched span
                print(string_id, "---->", span.text)

class Topic():
    def __init__(self,texte,language):
        self.texte=texte
        self.word2vec = KeyedVectors.load_word2vec_format("Gensim_models/frWac_no_postag_phrase_500_cbow_cut10.bin", binary=True, unicode_errors="ignore")
        self.keywords=Keywords(self.texte,language)
        self.keywords.clean()
        self.keywords.get()
    def get_topics(self):
        self.doc_list = []
        pr = self.keywords.words
        self.doc_list.append(pr)
        words = corpora.Dictionary(self.doc_list)

        # Turns each document into a bag of words.
        self.corpus = [words.doc2bow(doc) for doc in self.doc_list]
        self.lda_model = LdaModel(corpus=self.corpus,
                                           id2word=words,
                                           num_topics=2, 
                                           random_state=2,
                                           update_every=1,
                                           passes=100,
                                           alpha='auto',
                                           per_word_topics=False)
        print(self.lda_model.print_topics())
    def compare_to_word(self,word,seuil=0.05):
        liste_of_similars=[]
        ltmp=[]
        for w in self.keywords.words:
            try:
                similarity = self.word2vec.similarity(w, word)
            except:
                similarity=-INFINI
            if similarity>seuil and not(w in ltmp):
                liste_of_similars.append((similarity,w))
                ltmp.append(w)
        lout=sorted(liste_of_similars, key=lambda x: -x[0])   # sort by age
        print(word,"similarites trouvees avec les mots suivant",lout)
        return(lout)
    def analogy(self,lpos,lneg):
        result = self.word2vec.most_similar(positive=lpos, negative=lneg)
        return result
    def avg_sentence_vector(self,words, model, num_features, index2word_set):
        #function to average all words vectors in a given paragraph
        featureVec = np.zeros((num_features,), dtype="float32")
        nwords = 0

        for word in words:
            if word in index2word_set:
                nwords = nwords+1
                featureVec = np.add(featureVec, model[word])

        if nwords>0:
            featureVec = np.divide(featureVec, nwords)
        return featureVec
    def make_corpus(self,all_texts):
        texts=all_texts
        dictionary = Dictionary(texts)
        corpus = [dictionary.doc2bow(text) for text in texts]
        self.model_comparaison = LdaModel(corpus, id2word=dictionary, num_topics=10, minimum_probability=1e-8,iterations=200)
    def compare_two_sentences(self,c1,c2,comparison_method="WMD"):
        # sentence_1_avg_vector = self.avg_sentence_vector(s1.split(), model=self.word2vec, num_features=2,index2word_set=set(self.word2vec.wv.index2word))
        # sentence_2_avg_vector = self.avg_sentence_vector(s2.split(), model=self.word2vec, num_features=2,index2word_set=set(self.word2vec.wv.index2word))
        # sen1_sen2_similarity =  cosine_similarity(sentence_1_avg_vector,sentence_2_avg_vector)
        # return(sen1_sen2_similarity)
        print("comparing",c1[:5],c2[:5],"with",comparison_method)
        if comparison_method=="WMD":
            #self.word2vec.init_sims(True)
            distance = self.word2vec.wmdistance(c1, c2)
        else:
            
            tmp1 =  self.model_comparaison.id2word.doc2bow(c1)
            tmp2 =  self.model_comparaison.id2word.doc2bow(c2)
            tmp11 = self.model_comparaison[tmp1]
            tmp22 = self.model_comparaison[tmp2]
            if comparison_method=="HELLINGER":
                distance = hellinger(tmp11, tmp22)
            if comparison_method=="KULLBACK":
                distance = kullback_leibler(tmp11, tmp22)
            if comparison_method=="JACCARD":
                distance = jaccard(tmp11, tmp22)
        print("------> distance",distance)
        return(distance)

