from gensim.models import KeyedVectors
model = KeyedVectors.load_word2vec_format("Gensim_models/frWac_no_postag_phrase_500_cbow_cut10.bin", binary=True, unicode_errors="ignore")

word_vectors = model.wv

length=len(word_vectors.wv.vocab)
print("len",length)

# for word, vocab_obj in word_vectors.vocab.items():
#     print(word)

result = word_vectors.most_similar(positive=['toile','pattes'], negative=['gros'])
print("posneg",result)

print("hors liste",word_vectors.doesnt_match("homme femme enfant garçon fille maman papa repas".split()))
similarity = word_vectors.similarity('repas', 'manger')

print("similarite entre repas et manger",similarity)


result = word_vectors.similar_by_word("manger")
print("mots proches de manger",result)




sentence_obama = "Je n'aime pas le chien".lower().split()
sentence_president = "Je n'aime pas le chien noir".lower().split()
similarity = word_vectors.wmdistance(sentence_obama, sentence_president)
print(("phrases proches a",similarity))

sim = word_vectors.n_similarity(['maison', 'grande'], ['maison', 'petite'])
print("2 mots",sim)


exit()


import pprint
document = "Human machine interface for lab abc computer applications"
text_corpus = [
    "Human machine interface for lab abc computer applications",
    "A survey of user opinion of computer system response time",
    "The EPS user interface management system",
    "System and human system engineering testing of EPS",
    "Relation of user perceived response time to error measurement",
    "The generation of random binary unordered trees",
    "The intersection graph of paths in trees",
    "Graph minors IV Widths of trees and well quasi ordering",
    "Graph minors A survey",
]
# Create a set of frequent words
stoplist = set('for a of the and to in'.split(' '))
# Lowercase each document, split it by white space and filter out stopwords
texts = [[word for word in document.lower().split() if word not in stoplist]
         for document in text_corpus]
print(texts)
# Count word frequencies
from collections import defaultdict
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1

# Only keep words that appear more than once
processed_corpus = [[token for token in text if

 frequency[token] > 1] for text in texts]
print("processed corpus",processed_corpus)


from gensim import corpora

dictionary = corpora.Dictionary(processed_corpus)
print(dictionary)
print(dictionary.token2id)
new_doc = "Human computer interaction"
new_vec = dictionary.doc2bow(new_doc.lower().split())
print("new_vec",new_vec)

bow_corpus = [dictionary.doc2bow(text) for text in processed_corpus]
print("bow_corpus",bow_corpus)
from gensim import models

# train the model
tfidf = models.TfidfModel(bow_corpus)

# transform the "system minors" string
words = "system minors".lower().split()
print(tfidf[dictionary.doc2bow(words)])

from gensim import similarities

index = similarities.SparseMatrixSimilarity(tfidf[bow_corpus], num_features=12)

query_document = 'system engineering'.split()
query_bow = dictionary.doc2bow(query_document)
sims = index[tfidf[query_bow]]
print(list(enumerate(sims)))

for document_number, score in sorted(enumerate(sims), key=lambda x: x[1], reverse=True):
    print(document_number, score)


