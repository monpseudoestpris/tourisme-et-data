import nlp_functions



text=[x for x in open('puydufou.txt','r').readlines()]
t_to_parse=""
for t in text:
    t_to_parse=t_to_parse+t



m=nlp_functions.Match(t_to_parse)
m.define_rule("rules_fr.txt")
m.get()

topic=nlp_functions.Topic(t_to_parse,'fr')
topic.keywords.clean()
topic.keywords.get()
print(topic.analogy(  ["empereur","france"],[]  ))
topic.compare_to_word('parc')
topic.compare_to_word('loisir')
topic.compare_to_word('famille')
topic.compare_to_word('nature')
topic.compare_to_word('histoire')
topic.compare_to_word('archéologie')
topic.compare_to_word('kayak')
topic.compare_to_word('artifice')
topic.compare_to_word('manger')
topic.compare_to_word('Vendée')
topic.compare_to_word('Chouans')
topic.compare_to_word('cascade')
topic.compare_to_word('monarchie')
s1=topic.keywords.words
t2="Je veux aller dans un parc de loisirs en famille et voir des spectacles historiques et médiévaux et aller se promener en vendée"
t3="vacances en famille"
t4="parc d'attraction"





topic2=nlp_functions.Topic(t2,'fr')
topic2.keywords.clean()
topic2.keywords.get()
s2=topic2.keywords.words

topic3=nlp_functions.Topic(t3,'fr')
topic3.keywords.clean()
topic3.keywords.get()
s3=topic3.keywords.words

topic4=nlp_functions.Topic(t4,'fr')
topic4.keywords.clean()
topic4.keywords.get()
s4=topic4.keywords.words

all_text=[s1,s2,s3,s4]

print("s1",s1)
print("s2",s2)
print("s3",s3)
print("s4",s4)
#topic.make_corpus(all_text)

print("============================== DIFFERENT ========================")

# comparaison1=topic.compare_two_sentences(s1,s2,"HELLINGER")
# comparaison2=topic.compare_two_sentences(s1,s3,"HELLINGER")

# comparaison1=topic.compare_two_sentences(s1,s2,"KULLBACK")
# comparaison2=topic.compare_two_sentences(s1,s3,"KULLBACK")

# comparaison1=topic.compare_two_sentences(s1,s2,"JACCARD")
# comparaison2=topic.compare_two_sentences(s1,s3,"JACCARD")

comparaison1=topic.compare_two_sentences(s1,s2,"WMD")
comparaison2=topic.compare_two_sentences(s1,s3,"WMD")
comparaison1=topic.compare_two_sentences(s2,s3,"WMD")
comparaison2=topic.compare_two_sentences(s3,s4,"WMD")
comparaison2=topic.compare_two_sentences(s1,s4,"WMD")